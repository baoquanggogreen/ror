class CreateAuthorities < ActiveRecord::Migration[6.0]
  def change
    create_table :authorities do |t|
      t.numeric :IdAuth
      t.string :Role

      t.timestamps
    end
  end
end
