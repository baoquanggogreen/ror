class FixColumnName < ActiveRecord::Migration[6.0]
  def change
    rename_column :authorities, :IdAuth, :idAuth
    rename_column :authorities, :Role, :role
  end
end
