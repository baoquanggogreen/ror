class Authority < ApplicationRecord
    has_many :user, dependent: :destroy, inverse_of: :user
end
